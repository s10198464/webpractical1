﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_S10198464.Models
{
    public class Account
    {
        public string Message { get; set; }
        public Student Student { get; set; }
    }
    public class Student
    {
        public string Name { get; set; }
    }

}

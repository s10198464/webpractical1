﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Web_S10198464.Models
{
    public class Staff
    {

        [Display(Name = "ID")]
        public int StaffId { get; set; }


        
        [Required(ErrorMessage = "Must put a name"), MaxLength(50, ErrorMessage = "Cannot exceed 50 characters")]
        public string Name { get; set; }


       
        public char Gender { get; set; }


        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        [Display(Name = "Email Address")]
        [DisplayFormat(DataFormatString = @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }

        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00,10000.00, ErrorMessage = "Salary must be between 1.00 to 10000.00")]

        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }
    }
}

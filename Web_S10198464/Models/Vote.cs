﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;

namespace Web_S10198464.Models
{
    public class Vote
    {
        [Display(Name = "Book ID")]
        public int BookId { get; set; }
        [Display(Name = "Justification")]
        public string Justification { get; set; }
    }
}
